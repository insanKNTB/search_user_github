import React from 'react';

import {Provider} from "react-redux";
import { AppRouter } from "./AppRouter";

import { store } from "./store";

import './main.scss'

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <AppRouter/>
    </Provider>
  );
}

export default App;
